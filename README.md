# Running on Heroku

Create free account on [Heroku](https://heroku.com) platform.

Install [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli).

Make sure you `heroku login`.

## Deploying Rails app

### Prerequisites

- production database configuration `config/database.yml` will not be taken into account in favor of `DATABASE_URL` environment variable.

    ```yml
    production:
        username: <%= ENV.fetch("DB_USERNAME") %>
        password: <%= ENV.fetch("DB_PASSWORD") %>
        host: <%= ENV.fetch("DB_HOST") %>
    ```

    It should ignore specified production config values `adapter`, `database`, `username`, `password`, `host`, `port`

- Specify ruby version you want to use in your `Gemfile`:

    ```ruby
    ruby "2.4.4"
    ```

- **Images**

    - Image assets (eshop images of products) are created during rake task **spree_sample:load** which loads database with sample data. Heroku servers do not use persistent filesystem. It means images created during running this task will not be available to Heroku app afterwards. Images will need to be served from separate image storage.

    - Spree project provides integration with Amazon Web Services S3 storage. This eshop app is already configured to upload created images to S3.

    - You will need to have [AWS account](https://aws.amazon.com). Create and get [access keys](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html) for IAM user with rights for specific S3 bucket you will use.

    - Eshop app will use images from S3 storage (hyperlinks), not from app filesystem. It is required to fill environemnt variables listed in `config/application.yml`

    - Dependency for image creation is [Imagemagick](https://www.imagemagick.org/script/index.php) native program. Heroku provides Imagemagick installed by default.

    - You will need to make images in S3 bucket publicly accessible.

    - S3 bucket will need to have following CORS configuration (for processing images by Alexa app)

        ```xml
        <?xml version="1.0" encoding="UTF-8"?>
        <CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
            <CORSRule>
                <AllowedOrigin>http://ask-ifr-download.s3.amazonaws.com</AllowedOrigin>
                <AllowedMethod>GET</AllowedMethod>
            </CORSRule>
            <CORSRule>
                <AllowedOrigin>https://ask-ifr-download.s3.amazonaws.com</AllowedOrigin>
                <AllowedMethod>GET</AllowedMethod>
            </CORSRule>
        </CORSConfiguration>
        ```

### Deploy app

#### 1. Create heroku app

In project dorectory, run:

```
heroku create --region eu
```

- will create heroku app
- will add repository URI to your git remotes

```
heroku addons:create heroku-postgresql
```

- will provision postgresql databasea for your app
- should set `DATABASE_URL` variable in heroku environment

> You don't have to use heroku provisioned database
>
> Specify `DATABSE_URL` environment variable instead with protocol, credentials, host, port, database

#### 2. Configure heroku environment variables

App uses [Figaro ruby gem](https://github.com/laserlemon/figaro) for loading secrets to environment variables when app boots:

- copy sample `application.yml` file and fill values for keys.

    ```
    cp config/application.yml.sample config/application.yml
    ```

    > `config/application.yml` is listed in .gitconfig
    >
    > It should never be published, pushed or revealed

Create environment variables with sensitive information in heroku app environment:

- set environment variables in the heroku app from your command line:

    ```
    figaro heroku:set -e production
    ```

    > assuming you have figaro gem installed on your environment
    > - either by `gem install figaro`
    > - or `bundle install` in this app

#### 3. Deploy code to heroku

```
git push heroku master
```

- will update heroku git source code repository with your local git history
- will trigger app build

#### 4. Setup postgresql database and load data

```
heroku run rake db:migrate
```

- will create database schema (tables) from your db migrations

If you need to preload database with your initial data (safe for empty database), run:

```
heroku run rake db:seed
```

- will load database based on `db/seeds.rb`
- will create admin user on command line and **prompt for input**
    - enter username
    - enter password

```
heroku run rake spree_sample:load
```
- will load database with data from spree_sample gem
- includes processing images by imagemagick and uploading them to AWS S3 storage from your configuration

#### 5. Open app

```
heroku open
```

- will open app in your internet browser


# Running in Docker

TBD

TODO: environment safety

# Running on MacOS locally

* Install ruby

    This app was developed on ruby `2.4.2` - `2.4.4` versions.

    [Rbenv](https://github.com/rbenv/rbenv) can be used for managing ruby versions.

    ```
    brew install rbenv
    ```

* Install imagemagick

    > used for processing assets
    >
    > not required for running web app

    ```
    brew install imagemagick
    ```

    Native program required by paperclip gem (dependency used by `spree_core` to process images)

* Install bundler gem

    ```
    gem install bundler
    ```

* Install postgresql

    ```
    brew install postgresql
    ```

    [Homebrew services](https://github.com/Homebrew/homebrew-services) can be used for managing postgresql server locally.

    ```
    brew tap homebrew/services
    brew services start postgresql
    ```

    [PgAdmin client app](https://www.pgadmin.org) for example can also be useful for database management.

* Install gem dependencies

    ```
    cd project/
    bundle install
    ```

* Setup sensitive environment variables

    Copy sample application.yml file and fill values for keys.

    ```
    cp config/application.yml.sample config/application.yml
    ```

    Configuration is loaded to app environment on app start by figaro gem.

    > `config/application.yml` is listed in .gitconfig
    > It should never be published, pushed or revealed

* Set up and preload database

    ```
    cd project/
    bundle exec rails db:create
    bundle exec rails db:migrate
    bundle exec rails db:seed
    bundle exec rails spree_sample:load
    ```

* Precompile assets for performance

    ```
    cd project/
    bundle exec rails assets:precompile
    ```

* Start app

    ```
    cd project/
    rails s
    ```

# How was this app created (windows 10)

* install ruby

```
> ruby --version
ruby 2.4.2p198 (2017-09-14 revision 59899) [x64-mingw32]
```

I used chocolatey package manager

```
> cinst -y ruby
```

* install **rails** gem

  ...followed [medium beginners tutorial](https://medium.com/ruby-on-rails-web-application-development/how-to-install-rubyonrails-on-windows-7-8-10-complete-tutorial-2017-fc95720ee059)

  - but I used msys32 instead of msys2 for `ridk install`. I had msys32 on my machine. It worked.
  - pacman upgrade or system packages upgrade were tricky, I used some different command line parameters than tutorial
  - I did not install RubyGems manually.
  - I installed **Nokogiri**, **SQLite3** gems successfully.

```
> rails --version
Rails 5.1.4
```

* installed **ImageMagick** ImageMagick-6.9.6-Q16-HDRI
  * had troubles with version 7 on windows, uninstalled ImageMagick-7.0.7-8-Q8-x64-dll.exe
  * followed [medium beginners tutorial](https://medium.com/ruby-on-rails-web-application-development/install-rmagick-gem-on-windows-7-8-10-imagemagick-6-9-4-q16-hdri-5492c3fef202), although don't know if really needed to downgrade. I had troubles because of not configured project.

* `git clone <this_eshop>`

* `cd eshop`

* `bundle install`

  to get gem dependencies and install them to your environment (possibly includes compiling native dependencies)

* in file `config\initializers\paperclip.rb`:

adjust path to your ImageMagick installation directory

```ruby
Paperclip.options[:command_path] = 'C:\tools\ImageMagick-6.9.6-Q16-HDRI'
```

These commands modify this rails app, they needed to be un only once

* `rails g spree:install --user_class=Spree::User`
  `rails g spree:auth:install`
  `rails g spree_gateway:install`

Set up database
- specify pg in Gemfile and configure `config/database.yml` properly
- or specify sqlite3 in Gemfile and adjust `config/database.yml`

* `bundle exec rake db:create`
  `bundle exec rake db:migrate`
  `bundle exec rake db:seed`
  `bundle exec rake spree_sample:load`

Problematic task of spree sample rake was loading `assets`.
There is used paperclip gem, which uses commands of hosts' ImageMagick installation.

If successful, images will be created under `public\spree`.

* set environment variables

```
setx RAILS_SERVE_STATIC_FILES=y
setx SECRET_KEY_BASE=somethingrandomandverylong09af
setx RAILS_ENV=production
```

* `bundle exec rake assets:precompile`

It will create assets under `public\assets`.
Assets will be in **production mode** (minified js, css)

* `rails s`
  or `rails server`

will start server with production environment settings (because of environment variables set before).

* 🎉


<!--
# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
-->
