Spree::Api::V1::ProductsController.class_eval do
  def index
    if params[:ids]
      @products = product_scope.where(id: params[:ids].split(',').flatten)
    elsif params[:category_id]
      # find products by category id (taxon id)
      @products = Spree::Product.includes(:classifications).where('spree_products_taxons.taxon_id' => params[:category_id])
    elsif params[:category]
      # find products by category name (taxon name), ignore case
      @products = Spree::Product.joins(:taxons).where('spree_taxons.name ILIKE ?', "#{params[:category]}%")
    else
      @products = product_scope.ransack(params[:q]).result
    end

    @products = @products.distinct.page(params[:page]).per(params[:per_page])
    expires_in 24.hours, public: true
    headers['Surrogate-Control'] = "max-age=#{24.hours}"
    respond_with(@products)
  end
end
