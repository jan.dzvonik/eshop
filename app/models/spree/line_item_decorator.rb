Spree::LineItem.class_eval do
  def update_price_from_modifier(currency, opts)
    if currency
      self.currency = currency
      self.price = (variant.price_in(currency).amount || 0)  +
        variant.price_modifier_amount_in(currency, opts)
    else
      self.price = variant.price +
        variant.price_modifier_amount(opts)
    end
  end
end
