module Spree
  class Money
    def initialize(amount, options = {})
      @money = Monetize.parse([amount, (options[:currency] || Spree::Config[:currency])].join)
      @options = {}
      @options[:sign_before_symbol] = true
      @options[:symbol_position] = :after
    end
  end
end
