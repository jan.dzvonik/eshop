# This migration comes from spree_amazon_alexa (originally 20180104172814)
class CreateSpreeAlexaAccountLinkingSessions < SpreeExtension::Migration[4.2]
  def change
    create_table :spree_alexa_account_linking_sessions do |t|
      t.string :session_id
      t.string :state
      t.string :redirect_uri
      t.string :client_id
      t.string :response_type

      t.timestamps
    end
    add_index :spree_alexa_account_linking_sessions, :session_id
  end
end
